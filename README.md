# Blix Coding Test

In this exercise, you'll be asked to fetch time series data from an existing REST API, do some
simple aggregation on one of the metrics, and plot the aggregated numbers on a line chart.

## Data Source

We'll be fetching data from *data.gov.au* - the central source of Australian open government
data. The dataset we'll be using is *Yarra traffic counts*:

https://www.data.gov.au/dataset/ds-dga-427dbeab-de55-4230-8db8-4d3bde182ea1/details?q=council%20traffic

Please read the description on that page for details about the dataset.

More specifically, you'll be calling this endpoint to fetch the whole dataset in JSON format:

https://data.gov.au/data/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%229e26683b-6b30-424e-ace7-59047d811d1c%22

## Task Brief

We're going to look at only two data columns in that dataset:

- *date_captured*: this field specifies the year and the month
- *volume_per_day*: this field specifies the traffic volume per day during that month on a
specific road in a specific suburb

We'd like you to aggregate the *sum* of *volume_per_day* over *date_captured* to present
a report on the **average daily traffic volume captured per month** in the Yarra municipality.

The report should include a **table** and a **line chart**. The resulting line chart will look
something like this:

![Sample line chart](./line_chart.png)

## Requirements

You're required to write code in JavaScript, preferrably using Vue or React, to accomplish the
task. You can use any open source libraries as you see fit.

We'll be looking for these traits in you as a good candidate:

- **Code quality**: first and foremost, your code needs to be *modularised*, *consistent*, *clean*,
*tested* and appropriately *documented*
- **Comprehension of requirements**: ability to translate non-technical requirements into technical
specifications
- **Version control**: ability to use git properly, keep commits small and concise, and write
sensible commit messages
- **JavaScript and web skills**: JS/ES6/TypeScript, HTML, CSS and so on
- **An eye for good UI**: we don't expect you to be a designer but you'll need to know how to
create a clean and user-friendly UI

On the other hand, we are less concerned about exactly how your table or chart looks as long
as it looks nice and more importantly, user friendly. We also won't limit your delivery format,
which means you can put it on Github/Bitbucket, host it on your server, or send us a zip file.
